# Communication around Coq

Presenting Coq to a variety of audiences requires some slides to be prepared,
it would be nice to preserve some continuity between all these communication
opportunities

## A short presentation for management levels

subdirectory : industry-top-management-10mn

In this presentation, the theory of Coq is only lightly mentioned.  On the
other hand, Compcert is used as an illustrative example, and then the aspects
that are relevant for industrial users are underlined.

## A one page presentation for consumers of products that were developed using Coq (in French)

subdirectory : presentation-grand-public

The requestion for this document was to produce a one page document explaining
what Coq is, including MetaCoq and verified extraction.  The target audience
are the consumers of a company that develops their software using Coq
(but whose clients will not need to use Coq).

The sources are in file
[Coq-MetaCoq-VerifiedExtraction.md](presentation-grand-public/Coq-MetaCoq-VerifiedExtraction.md).
This is a markdown file, that can be read directly on the git web
site.  to produce a pdf file, the following sequence of operations works.

```
pandoc -s -t latex Coq-MetaCoq-VerifiedExtraction.md -o Coq-MetaCoq-VerifiedExtraction.tex
pdflatex Coq-MetaCoq-VerifiedExtraction.tex
```

In the current version, the document has 2 pages.