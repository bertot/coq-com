## Le système Coq fournit un langage de programmation adapté pour éviter les erreurs de programmation au maximum

Les langages de programmation sont conçus pour détecter certaines classes
d'erreurs commises par les développeurs lorsqu'ils expriment les tâches qui
doivent être effectuées par les ordinateurs.

Une technique habituelle est d'imposer dans le langage une discipline
de typage, où les développeurs déclarent quel type d'entrées
chaque sous-programme attend et quel type de résultats ce programme
produit.  Un système automatique, le vérificateur de types, permet
ensuite de vérifier que les différents composants d'un logiciels sont
assemblés de façon cohérente, en respectant les informations de
typage.

Il existe plusieurs disciplines de typage, les plus conventionnelles
permettent seulement de distinguer entre des catégories grossières de
données: les nombres, les caractères, les images, par exemple.

Le système Coq fournit un langage avec une discipline de typage plus
puissante que les langages conventionnels.  D'une part, le type d'un
objet peut exprimer des propriétés mathématiques très fines (par
exemple, le résultat de ce calcul est une séquence de nombres dans
l'ordre croissant), d'autre part le type d'un programme
permet d'exprimer que les entrées et les sorties sont tenues de respecter une
relation précise (par exemple, tous les nombres fournis dans la sortie
étaient présents dans l'entrée).

Grâce à une similitude très forte entre programmes et preuves mathématiques,
le vérificateur de type de Coq peut également être utilisé pour vérifier
qu'une preuve mathématique est correcte, avec une certitude absolue.

## Une discipline de typage pour la programmation et la spécification

Grâce à cette discipline de typage, les développeurs peuvent décrire à
la fois de façon précise comment un programme doit fonctionner
(l'algorithme du programme) et quelles sont les propriétés garanties
pour les résultats de ces programmes (la **spécification** du programme).
Grâce au vérificateur de types, le système Coq permet de vérifier que
le programme est assemblé de façon cohérente, en respectant les
spécifications attendues par les utilisateurs.

En pratique, un travail d'expert est nécessaire.  En premier lieu, la
spécification doit exprimer sans ambigüité ce que les utilisateurs du
programme attendent comme propriétés.  Ensuite, l'algorithme décrivant
les opérations effectuées doit être décrit de façon précise.  Souvent,
une troisième phase est nécessaire pour produire une preuve que
l'algorithme est bien conçu pour satisfaire la spécification.  Cette
preuve correspond à vérifier une fois pour toutes que la spécification
est satisfaite dans tous les cas possibles d'exécution.

Lorsque ce travail de preuve est terminé, on a la garantie, fournie
par le vérificateur de types, que le programme fait toujours ce qui
est spécifié.  Cette garantie est plus forte que
ce que l'on obtient par les méthodes de test traditionnelles.

## Une chaine de production de logiciel à forte garanties de correction

Le langage de programmation de Coq est un langage de haut niveau, qui
doit être traduit en un langage de bas niveau pour être exécuté par
les ordinateurs.

Un traducteur de langage de haut niveau vers le bas niveau est
également un programme.  Ce traducteur peut lui-même être
décrit à l'aide du langage de Coq : on parle alors d'un
meta-programme, qui prend en entrée des programmes et produit des
programmes.

Le système Coq fournit un outil appelé MetaCoq dans lequel ces
meta-programmes peuvent être décrits avec leur spécification, et avec
lequel les concepteurs du système ont prouvé que la spécification est
satisfaite par le programme, la preuve étant vérifiée par Coq.
MetaCoq a également été utilisé pour vérifier que le vérificateur de
types de Coq est correct (avec une preuve vérifiée par Coq).
L'ensemble de la chaine de production de logiciel a donc été décrite
et vérifiée plusieurs fois.

Avec ces vérifications multiples, un programme ne peut contenir de
fautes que si la spécification est incorrecte, si le logiciel Coq
contient une erreur, ce qui est improbable car il est lui-même
vérifié, ou si le compilateur du langage OCaml est incorrect.  La
probabilité qu'une erreur existe est donc beaucoup plus faible que
pour les autres approches de développement logiciel.
